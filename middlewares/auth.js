const { decryption } = require("../helper/encrypt");
const { User } = require("../models");
module.exports = {
  authorization: async (req, res, next) => {
    try {
      if (!req.headers.authorization) {
        res.status(401).json({
          message: "Unauthorized",
        });
      } else {
        const username = decryption(req.headers.authorization);

        const user = await User.findOne({
          attributes: ["username"],
          where: {
            username: username,
          },
        });
        console.log(`user: ${username}`);
        if (user) {
          next();
        } else {
          res.status(401).json({
            message: "Unauthorized",
          });
        }
      }
    } catch (error) {
      next(error);
    }
  },
};
