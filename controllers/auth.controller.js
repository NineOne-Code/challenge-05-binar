"use strict";
const { encryption, decryption } = require("../helper/encrypt");
const { User } = require("../models");

module.exports = {
  signIn: async (req, res) => {
    try {
      const { username, password } = req.body;
      const user = await User.findOne({
        attributes: ["id", "username"],
        where: {
          username: username,
          password: password,
        },
      });
      // console.log(user);
      const encrypt = encryption(user.username);
      res.status(201).json({
        message: `User ${user.username} is available, with ID ${user.id}.`,
        token: encrypt,
      });
    } catch (error) {
      if (
        error.message === `Cannot read property 'username' of null` ||
        error.message ===
          `WHERE parameter \"username\" has invalid \"undefined\" value`
      ) {
        return res.status(404).json({ message: "Username Not Found" });
      }
      if (
        error.message ===
        `WHERE parameter \"password\" has invalid \"undefined\" value`
      ) {
        return res.status(403).json({ message: "Wrong Password" });
      }
      res
        .status(500)
        .json({ message: error.message || "Internal Server Error" });
      console.log(error);
    }
  },
  signUp: async (req, res) => {
    try {
      const { username, password } = req.body;
      const user = await User.create({
        username: username,
        password: password,
      });
      const encrypt = encryption(user.username);
      res.status(201).json({
        message: `Welcome User ${user.username} to We're server, your ID is ${user.id}.`,
        token: encrypt,
      });
    } catch (error) {
      if (error.errors[0].message === `username must be unique`) {
        return res.status(409).json({ message: "Username already registered" });
      }
      res
        .status(500)
        .json({ message: error.errors[0].message || "Internal Server Error" });
      console.log(error);
    }
  },
};
