const express = require("express");
const {
  deleteAccount,
  getAllUsers,
} = require("../controllers/user.controller");
const { authorization } = require("../middlewares/auth");
const biodataRoute = require("../routes/userbiodata.routes");

var router = express.Router();

router.use(authorization);

router.get("/", getAllUsers);
router.delete("/delete", deleteAccount);

router.use("/myaccount", biodataRoute);

module.exports = router;
