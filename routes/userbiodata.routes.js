const express = require("express");
const {
  addData,
  deleteData,
  getUser,
  updateData,
} = require("../controllers/userbiodata.controller");
const { authorization } = require("../middlewares/auth");

var router = express.Router();

// router.use(authorization);

router.get("/", getUser);
router.post("/add", addData);
router.delete("/delete", deleteData);
router.put("/update", updateData);
module.exports = router;
